import td4 # indiquer ici le nom de votre module
import unittest

class TestTd4Names(unittest.TestCase):
    def assertHasAttr(self, obj, name, question):
        if not hasattr(obj, name):
            raise self.failureException(f"Q{question}: {name} n'est pas un attribut de {obj.__name__}")

    def test_names(self):
        arguments = [ (td4, 'Hashtable', 1),
                      (td4, 'hash_naive', 2),
                      (td4.Hashtable, '__init__', 2),
                      (td4.Hashtable, 'set', 2),
                      (td4.Hashtable, 'get', 3),
                      (td4.Hashtable, 'repartition', 4),
                      (td4, 'hash_jenkins', 5),
                      (td4.Hashtable, 'resize', 6),
                      (td4, 'HashtableRobinHood', 8),
                      (td4.Hashtable, '__getitem__', 9),
                      (td4.Hashtable, '__setitem__', 9) ]
        for obj, name, question in arguments:
            with self.subTest(msg=f"{obj.__name__} a l'attribut {name}"):
                self.assertHasAttr(obj, name, question)

if __name__ == '__main__':
    unittest.main()
