import re
import importlib
import ast
import unittest
import sys


class TestLinkedList(unittest.TestCase):
    def test_question_1(self):
        L0 = td3.LinkedList(None, None)
        L1 = td3.LinkedList(3, L0)
        L2 = td3.LinkedList(5, L1)
        self.assertTrue(L0.is_empty())
        self.assertFalse(L1.is_empty())
        self.assertIn('3', str(L1))
        self.assertIn('3', str(L2))
        self.assertIn('5', L2.__repr__())
    
    def test_question_2(self):
        L0 = td3.LinkedList.create_empty()
        self.assertIsNone(L0._LinkedList__value, None)
        self.assertIsNone(L0._LinkedList__next, None)
        L1 = td3.LinkedList(3, L0)
        L2 = td3.LinkedList(15, L1)
        size = L2.size() if hasattr(L2,'size') else len(L2)
        self.assertEqual(size, 2)
    
    def test_question_3(self):
        L = td3.LinkedList.create_list([15,4,3,2,1])
        for element in [15,4,3,2,1]:
            self.assertEqual(L._LinkedList__value, element)
            L = L._LinkedList__next
        self.assertIsNone(L._LinkedList__value)
    
    def test_question_4(self):
        L0 = td3.LinkedList(None, None)
        L1 = td3.LinkedList(3, L0)
        L2 = td3.LinkedList(25, L1)
        try:
            L = L2.append(1)
        except:
            L = L2.__append__(1)
            print("Warning wrong name")
        self.assertNotIn('1', str(L2))
        self.assertIsNotNone(re.search('25.*3.*1', str(L)))
    
    def test_question_5(self):
        L0 = td3.LinkedList(None, None)
        L1 = td3.LinkedList(3, L0)
        L2 = td3.LinkedList(15, L1)
        N0 = td3.LinkedList(None, None)
        N1 = td3.LinkedList(3, L0)
        N2 = td3.LinkedList(15, L1)
        self.assertEqual(L2, N2)
    
    def test_question_6(self):
        L = td3.LinkedList(None, None)
        for e in [1,2,3,4,15]:
            L = td3.LinkedList(e, L)
        R = L.reverse()
        self.assertIsNotNone(re.search('15.*4.*3.*2.*1', str(L)))
        self.assertIsNotNone(re.search('1.*2.*3.*4.*15', str(R)))
    
    def test_question_7(self):
        L = td3.LinkedList(None, None)
        for e in [1,2,4,3,4,15]:
            L = td3.LinkedList(e, L)
        D = L.delete(4)
        self.assertIsNotNone(re.search('15.*4.*3.*4.*2.*1', str(L)) is not None)
        self.assertIsNotNone(re.search('15.*3.*4.*2.*1', str(D)))
        size = D.size() if hasattr(D,'size') else len(D)
        self.assertEqual(size, 5)
    
    def test_question_8(self):
        L = td3.LinkedList(None, None)
        for e in [4,17,4,2,4,2,1]:
            L = td3.LinkedList(e, L)
        max_multiplicity = [s for s in dir(L) if re.search('ax', s)]
        self.assertNotEqual(max_multiplicity, [])
        self.assertEqual(getattr(L, max_multiplicity[-1])(), 3)
    
    def test_question_9(self):
        L = td3.LinkedList(None, None)
        for e in [4,17,4,2,4,2,1]:
            L = td3.LinkedList(e, L)
        S = L.merge_sort()
        self.assertIsNotNone(re.search('1.*2.*2.*4.*4.*4.*17', str(S)))
        size = S.size() if hasattr(S,'size') else len(S)
        self.assertEqual(size, 7)

def strip_module(filename):
    with open(filename, 'rb') as f:
       p = ast.parse(f.read())
    for node in p.body[:]:
        if not isinstance(node, (ast.FunctionDef, ast.ClassDef, ast.Assign,
            ast.Import, ast.ImportFrom)):
            p.body.remove(node)
    spec = importlib.util.spec_from_loader('td3', loader=None)
    td3  = importlib.util.module_from_spec(spec)
    code = ast.unparse(p)
    exec(code, td3.__dict__)
    return td3

if __name__ == '__main__':
    filename = sys.argv[1]
    td3 = strip_module(filename)
    unittest.main(argv=[''])

